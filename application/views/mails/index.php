<!DOCTYPE html>
<html>
<head>
	<title>Mail</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
</head>
<body>
	<form action="<?php echo base_url('MailController/send_mail') ?>" method="POST">
		<?php echo validation_errors(); ?>
		<label>sender</label>
		<input type="email" name="sender_mail">
		<br>
		<label>receiver</label>
		<input type="email" name="receiver_mail">
		<br>
		<button type="submit">Send</button>
	</form>
	<script src="<?php echo base_url('assets/js/bootstrap.js'); ?>"></script>

	<?php if (!empty($this->session->set_flashdata('success_msg'))) { ?>
		# code...
	<?php } ?> 
</body>
</html>