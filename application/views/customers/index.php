<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<title>Hello, world!</title>
	<style type="text/css">
		.error{
			color: red;
		}
	</style>
</head>
<body>
	

	<div class="container">
		<div class="row">
			<head>
				<h1>Complete your information</h1>
			</head>
			
			
		</div>
		<div class="row">
			<nav>
				<div class="error"><?php echo validation_errors(); ?> </div>
			</nav>
		</div>
		<div class="row">
			<content>
				<form action="<?php echo base_url('customerController/submit'); ?>" method="POST">
					<div class="form-group row">
						<label for="example-text-input" class="col-2 col-form-label">Name:</label>
						<div class="col-10">
							<input class="form-control" type="text" value="" id="example-text-input" name="cname" placeholder="Name">
						</div>
					</div>
					
					<div class="form-group row">
						<label for="example-tel-input" class="col-2 col-form-label">Tel:</label>
						<div class="col-10">
							<input class="form-control" type="tel" value="" id="example-tel-input" name="cmobile" placeholder="10 Digit Mobile No.">
							<span style="color: #6a737c"> example: 0962358168 </span>
						</div>
					</div>
					
					<div class="form-group row">
						<div class="col-2">
							<input type="submit" name="" value="Save" class="btn btn-success">
						</div>
					</div>
					
				</form>
			</content>	
		</div>

	</div>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>