<center>
	<a href="<?php echo base_url('staffController/index'); ?>">Back</a>


	<?php echo validation_errors(); ?>
	<?php echo form_open('staffController/update/'.$staff->id); ?>
	<?php
		echo form_input(array('type'=>'hidden','name'=>'id','value'=>$staff->id));
		echo form_label('name: ','name');
		echo form_input(array('type'=>'text','name'=>'name','value'=>$staff->StaffName));
		echo "<br>";
		$gender = [
			'male'=>'Male',
			'female'=>'Female'
		];
		echo form_label('Gender:');
		echo form_dropdown('gender',$gender,$staff->gender);
		echo "<br>";
		$email = array('type' =>'email','name'=>'email','value'=>$staff->email );
		echo form_label('Email:');
		echo form_input($email);
		echo "<br>";
		echo form_label('Phone:');
		$phone = array('type'=>'text','name'=>'phone','value'=>$staff->phone);
		echo form_input($phone);
		echo "<br>";
		echo form_label('website');
		echo form_input(array('type'=>'text','name'=>'website','placeholder'=>'enter website','value'=>$staff->website));
		echo "<br>";
		echo form_label('address:');
		echo form_input(array('type'=>'text','name'=>'address','value'=>$staff->address));
		echo "<br>";
		echo form_submit('staffsubmit','submit');
		echo form_close();
	 ?>

</center>