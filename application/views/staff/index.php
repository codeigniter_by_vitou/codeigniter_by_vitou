
<center>
	
	<?php
	if($this->session->flashdata('success_msg'))
	{
		?>
		<div class="alert alert-success">
			<?php echo $this->session->flashdata('success_msg'); ?>
		</div>
		<?php		
	}
	?>
	<?php
	if($this->session->flashdata('error_msg'))
	{
		?>
		<div class="alert alert-Warning">
			<?php echo $this->session->flashdata('error_msg'); ?>
		</div>
		<?php		
	}
	?>
	<div id="show_table">
		<a href="<?php echo base_url('companyController/index'); ?>">Back</a>
		<?php
		echo form_open('staffController/search');
		echo form_input(array('type'=>'text','name'=>'search_staff'));
		echo form_submit('search','search');
		echo form_close();
		?>
		<?php echo "<br>"; ?>
		<?php
			// Set a table template to specify the design of table layout
		$table_open = array(
			'table_open' => '<table cellpadding="2" cellspacing="1" class="table_show" border="1">');
		$this->table->set_template($table_open);
		$checkedAll =array(
			'data'=>'checkall'.'<input type="checkbox" id="checkedAll">'
		);
		$id = array('data'=> 'Id');
		$name = array('data' => 'name', 'rowspan' => 1,);
		$gender = array('data' => 'Gender', 'colspan' => 1);
		$email = array('data' =>'Email', 'rowspan'=> 1);
		$phone = array('data' =>'Phone', 'rowspan'=> 1);
		$website = array('data' => 'Website');
		$address = array('data' => 'Adress' );
		$company = array('data'=>'company');
		$action = array('data'=>'Action');
		$this->table->add_row($checkedAll,$id,$name, $gender, $email,$phone, $website, $address,$company,$action);
		?>
		<!-- get record -->
		<?php echo form_open('staffController/deleteAll'); ?>
		<?php
		if (isset($staffs)) {
			foreach( $staffs as $staff){
				$checkSingle = array(
					'data'=>'<input type="checkbox" name="checkbox[]" class="checkSingle" value="'.$staff->id.'">');
				$id = array('data' => $staff->id);
				$name = array('data'=> $staff->StaffName);
				$gender = array('data'=> $staff->gender);
				$email = array('data'=> $staff->email);
				$phone = array('data'=> $staff->phone);
				$website = array('data'=> $staff->website);
				$address = array('data' => $staff->address);
				$company = array('data' => $staff->CompanyName);
				$action = array(
					'data'=>'<a href="'.base_url('staffController/view/'.$staff->id).'">view</a>|'.
					'<a href="'.base_url('staffController/edit/'.$staff->id).'">edit</a>|'.
					'<a href="'.base_url('staffController/delete/'.$staff->id).'" 
					onclick="return confirm_delete()">delete</a>'
				);
				$this->table->add_row($checkSingle,$id, $name,$gender,$email, $phone, $website,$address,$company,$action);
			}
		}
		// else
		// {
		// 	$noRecord = array('data'=>'<center><h2>No record</h2></center>','colspan'=>8);
		// 	$this->table->add_row($noRecord);
		// }
		// search
		if (isset($search_staff)) {
			foreach( $search_staff as $staff){
							// foreach ($staff as $company) {					
				$checkSingle = array(
					'data'=>'<input type="checkbox" name="checkbox[]" class="checkSingle" value="'.$staff->id.'">');
				$id = array('data' => $staff->id);
				$name = array('data'=> $staff->StaffName);
				$gender = array('data'=> $staff->gender);
				$email = array('data'=> $staff->email);
				$phone = array('data'=> $staff->phone);
				$website = array('data'=> $staff->website);
				$address = array('data' => $staff->address);
				$action = array(
					'data'=>'<a href="'.base_url('staffController/view/'.$staff->id).'">view</a>|'.
					'<a href="'.base_url('staffController/edit/'.$staff->id).'">edit</a>|'.
					'<a href="'.base_url('staffController/delete/'.$staff->id).'" 
					onclick="return confirm_delete()">delete</a>'
				);
				$this->table->add_row($checkSingle,$id, $name,$gender,$email, $phone, $website,$address,$action);
			}
		}

		echo $this->table->generate();
		?>
		<?php echo form_submit('delete','DeleteAll'); ?>
		<?php echo form_close(); ?>	
	</div>
</center>



