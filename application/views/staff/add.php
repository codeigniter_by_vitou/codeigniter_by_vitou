<center>
	<a href="<?php echo base_url('companyController/index'); ?>">Back</a>


	<?php echo validation_errors(); ?>

	<?php echo form_open('staffController/store'); ?>
	<?php
		echo form_hidden(array('company_id'=>'1'));
		$data = [
			'name' => 'name'
		];
		echo form_label('name: ','name');
		echo form_input($data);
		echo "<br>";
		$gender = [
			'male'=>'Male',
			'female'=>'Female'
		];
		echo form_label('Gender:');
		echo form_dropdown('gender',$gender);
		echo "<br>";
		$email = array('type' =>'email','name'=>'email' );
		echo form_label('Email:');
		echo form_input($email);
		echo "<br>";
		echo form_label('Phone:');
		$phone = array('type'=>'text','name'=>'phone');
		echo form_input($phone);
		echo "<br>";
		echo form_label('website');
		echo form_input(array('type'=>'text','name'=>'website','placeholder'=>'enter website'));
		echo "<br>";
		echo form_label('address:');
		echo form_input(array('type'=>'text','name'=>'address'));
		echo "<br>";
		echo form_submit('staffsubmit','submit');
		echo form_close();
	 ?>

</center>