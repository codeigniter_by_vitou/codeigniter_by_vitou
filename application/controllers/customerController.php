<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

	class customerController extends CI_Controller{

		function __construct()
		{
			parent::__construct();
			$this->load->model('customer_model');
		}
		public function index()
		{
			
			
				$this->load->view('customers/index');
			
		}

		function submit()
		{

			$this->form_validation->set_rules('cname','Name','required|min_length[5]|max_length[15]');
			$this->form_validation->set_rules('cmobile','Mobile','required|regex_match[/^[0-9]{10}$/]');
			if ($this->form_validation->run() == FALSE) {
				$this->load->view('customers/index');
			}else{
				$this->customer_model->submit_customer();
			}
			
		}
		

	}



 ?>