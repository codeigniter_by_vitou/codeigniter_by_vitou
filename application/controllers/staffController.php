<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
	/**
	 * 
	 */
	class staffController extends CI_Controller
	{
		
		function __construct()
		{
			parent::__construct();	
			$this->load->model('staff_m','m');
			$this->load->model('company_m');
		}
		public function index()
		{
			$data['staffs'] =  $this->m->get_specified_staff();
			$data['company'] = $this->company_m->get_company_data();
			$this->load->view('layout/header');
			$this->load->view('staff/index',$data);
			$this->load->view('layout/footer');
		}

		public function add()
		{
			$this->load->view('layout/header');
			$this->load->view('staff/add');
			$this->load->view('layout/footer');
			
		}
		public function store()
		{
			$this->form_validation->set_rules('name','Name','required');
			$this->form_validation->set_rules('gender','Gender','required');
			$this->form_validation->set_rules('email','Email','required');
			$this->form_validation->set_rules('phone','Phone','required');
			$this->form_validation->set_rules('website','Website','required');
			$this->form_validation->set_rules('address','Address','required');
			$this->form_validation->set_rules('company_id','Company','required');

			if ($this->form_validation->run() == FALSE) {
				$this->load->view('staff/add');
			}else{

				$data = $this->m->store();
				$this->session->set_flashdata('success_msg', 'Record added successfully');
				redirect(base_url('companyController/index'));
			}
			
		}
		public function edit($id)
		{
			$data['staff'] = $this->m->editInfo($id);
			$this->load->view('staff/edit',$data);

		}
		// update
		public function update()
		{
			
			$this->m->updateInfo();
			$this->session->set_flashdata('success_msg','Record updated successfully');
			redirect(base_url('staffController/index'));
		
		}
		// delete
		public function delete($id)
		{
			$this->m->deleteInfo($id);
			$this->session->set_flashdata('success_msg', 'Record deleted successfully');
			redirect(base_url('staffController/index'));
		}
		// view
		public function view($id)
		{
			$data['staff'] =  $this->m->viewInfo($id);
			$this->load->view('layout/header');
			$this->load->view('staff/view',$data);
			$this->load->view('layout/footer');
		}
		//search
		public function search()
		{
			$data['search_staff'] = $this->m->searchInfo();
			$this->load->view('staff/index',$data);
			
		}
		// delete all
		public function deleteAll()
		{
		
			$this->m->deleteAllCheck();
			$this->session->set_flashdata('success_msg','Delete successfully');
			redirect(base_url('staffController/index'));
		
		}

	}


	?>