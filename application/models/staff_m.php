<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class staff_m extends CI_Model{

	

		// get record
	public function get_specified_staff()
	{
		$this->db->select('tbl_staff.*,tbl_company.CompanyName');
		$this->db->from('tbl_company');
		$this->db->join('tbl_staff', 'tbl_company.id = tbl_staff.company_id');
		$q = $this->db->get();
		return $q->result();

	}
		// store record
	public function store()
	{
		$data = array(
			'StaffName' =>$this->input->post('name'),
			'gender' =>$this->input->post('gender'),
			'email' =>$this->input->post('phone'),
			'phone' =>$this->input->post('phone'),
			'website' =>$this->input->post('website'),
			'address' =>$this->input->post('address'),
			'company_id' =>$this->input->post('company_id'),
		);
		$store = $this->db->insert('tbl_staff',$data);
		if ($store) {
			$this->session->set_flashdata('sucess-msg','Success!');
		}

	}
		// edit record
	public function editInfo($id)
	{
		$this->db->select('tbl_staff.*,tbl_company.CompanyName');
		$this->db->from('tbl_company');
		$this->db->join('tbl_staff', 'tbl_company.id = tbl_staff.company_id');
		$q = $this->db->get();
		return $q->row();
	}
		// update record
	public function updateInfo()
	{
		$id = $this->input->post('id');
		$field = array(
			'StaffName' =>$this->input->post('name'),
			'gender' =>$this->input->post('gender'),
			'email' =>$this->input->post('email'),
			'phone' =>$this->input->post('phone'),
			'website' =>$this->input->post('website'),
			'address' =>$this->input->post('address'),
		);
		$this->db->where('id',$id);
		$aa = $this->db->update('tbl_staff',$field);

	}
		// delete
	public function deleteInfo($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('tbl_staff');

	}
		// view
	public function viewInfo($id)
	{
		$this->db->select('tbl_staff.*,tbl_company.CompanyName');
		$this->db->from('tbl_company');
		$this->db->join('tbl_staff', 'tbl_company.id = tbl_staff.company_id');
		$q = $this->db->get();
		return $q->row();
	}
	// search
	public function searchInfo()
	{
		$q=$this->input->post('search_staff');
		$this->db->like('StaffName',$q);
		$this->db->or_like('phone',$q);
		$this->db->or_like('email',$q);
		$this->db->or_like('address',$q);
		$this->db->or_like('website',$q);
		$data = $this->db->get('tbl_staff');

		if ($data->num_rows()<=0 || $q == null)
		{
			$this->session->set_flashdata('error_msg','search not found');
			redirect(base_url('staffController/index'));
		}
		else
		{
			return $data->result();
		}
	}
	// delete all
	public function deleteAllCheck()
	{
		$data = $this->input->post('checkbox');
		foreach ($data as $id ) {
			$this->db->where('id',$id);
			$this->db->delete('tbl_staff');
		}

	}

}


?>