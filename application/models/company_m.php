<?php 

	class company_m extends cI_Model{

		public function get_company_data()
		{
			$data = $this->db->get('tbl_company');
			if ($data->num_rows() > 0) {
				
				return $data->row();
			}
		}
		public function count_staff()
		{	
			$this->db->select('*');
			$this->db->from('tbl_staff');
			$this->db->join('tbl_company', 'tbl_staff.company_id = tbl_company.id');
			$q = $this->db->get();
			return $q->num_rows();
		}
	}


 ?>